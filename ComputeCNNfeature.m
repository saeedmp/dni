function Feat = ComputeCNNfeature(Dist,net)


lay = net.Layers;

% layer numbers for five CNN blocks of Resnet
relu_resnet = [5 17 79 141 173]; %Resnet
eps=1e-12;
wpSize=224;
cnt=0;


[wi ,le , ~] = size(Dist);

Feat = [];

% Block segmentation
for ii=1:112:wi
    for jj=1:112:le

        if(ii<wi-wpSize-1 && jj<le-wpSize-1)
            d1 = Dist (ii:ii+wpSize-1, jj:jj+wpSize-1,:);
            cnt = cnt+1;

            %Making a phase scrambled image
            Ref1_c= d1(:,:,1);
            raw = fft2(double(Ref1_c));
            avgMag = abs(raw); % get average magnitude

            randImg = rand(size(avgMag));
            imFreq = fft2(double(randImg));

            cmplxIm = avgMag.*exp(1i.*angle(imFreq));
            outImage = ifft2(cmplxIm, 'symmetric');
            sc_Ref1 = uint8(filterImg(outImage));


            sc_Ref = cat(3,sc_Ref1,sc_Ref1,sc_Ref1);

            % index of the CNN block 3
            ind = relu_resnet(1,3);
            laynom = lay(ind,1).Name;
            % Features of input image
            act_Ref = activations(net,d1,laynom);

            % Features of scrambled image
            act_Ref2 = activations(net,sc_Ref,laynom);

            % comput modulation index
            cc = (act_Ref - act_Ref2)./(act_Ref + act_Ref2 +eps);

            % thresholding features
            act_Ref (cc<0.05) = 0;


            % computing standard deviation of features
            s1 = std(act_Ref , [], [1 2]);
            s1=permute(s1,[3 2 1]);

            Feat(:,cnt) = s1;


        end
    end
end