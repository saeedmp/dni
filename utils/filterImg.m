% Borrowed from the work of Peter J Kohleret al.,...
% Representation of maximally regular textures in human visual cortex. Journal of Neuroscience, 36(3):714–729, 2016
function outImg = filterImg(inImg)

% Make filter intensity adaptive (600 is empirical number)
sigma = 1/60;
lowpass = fspecial('gaussian', [9 9], sigma);

% filter
image = imfilter(inImg, lowpass);

% histeq
image = double(histeq(uint8(image)));

% normalize
image = (image)./range(image(:)); %scale to unit range
image = image - mean(image(:)); %bring mean luminance to zero
image = image/max(abs(image(:))); %Scale so max signed value is 1
outImg = 125*image+127; % Scale into 2-252 range
end