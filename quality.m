
% Path for the PCA implementation of IL-NIQE paper
addpath('./utils')


%Load features of the pristine dataset in five groups
x11 = load('./Features/Animal.mat');
x22 = load('./Features/landscape.mat');
x33 = load('./Features/nature.mat');
x44 = load('./Features/object.mat');
x55 = load('./Features/people.mat');

% Concatenate Features
L3_all = [x11.f33,x22.f33,x33.f33,x44.f33,x55.f33];


%%params
nanConst = 2000;
reservedRatio = 0.99;

% Apply PCA
[principleVectors, meanOfSampleData,projectionOfTrainingData] = MyPCA(L3_all,reservedRatio);
prisparam = projectionOfTrainingData';

% Build MVG model of prisitne images (mean and covariance calculation)
mu_L3       = nanmean(prisparam);
cov_L3      = nancov(prisparam);


% load Resnet
net = resnet50();

% Read image
Dist = imread('dist.bmp');

%compute deep features
Feat = ComputeCNNfeature(Dist, net);

%PCA on test (distroted) image
meanMatrix = repmat(meanOfSampleData,1,size(Feat,2));
coefficientsViaPCA = principleVectors'*(Feat - meanMatrix);
distparam = coefficientsViaPCA';


% MVG model of test (distorted) image
mu_distparam     = nanmean(distparam);
cov_distparam    = nancov(distparam);
nanIndicator = isnan(mu_distparam);
mu_distparam(nanIndicator) = nanConst;
nanIndicator = isnan(cov_distparam);
cov_distparam(nanIndicator) = nanConst;

invcov_param = pinv((cov_L3+cov_distparam)/2);

dist = [];

for indexFeature = 1:size(distparam,1)
    currentFea = distparam(indexFeature,:);
    nanIndicator = isnan(currentFea);
    currentFea(nanIndicator) = mu_distparam(nanIndicator);

    % compute distance
    thisDist = sqrt((currentFea - mu_L3)*invcov_param*(currentFea - mu_L3)');
    dist = [dist;thisDist];
end



% Compute final quality
qualityscore= mean(dist)


