# Deep Naturalness Index



## Getting started

The source code is an implementation of the deep naturalness index (DNI) for opinion-unaware no-reference image quality assessment. Run the script 'quality.m' to obtain a quality score for the input image.  

## Usage
If you use this software for your research, please cite the following paper:

- Saeed Mahmoudpour and Peter Schelkens, "Revisiting natural scene statistical modeling using Deep features for opinion-unaware Image Quality Assessment", International Conference on Image Processing, 2022.
